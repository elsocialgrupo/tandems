# TANDEMS

This is for all the people who want to create a modern webframe work based
on the best in class technologies. This is an update on the **MEAN** stack.

**T**ypescipt
**A**ngular
**N**odeJS
**D**ynamoDb
**E**xpress
**M**ocha
**S**wagger

**Credits**
[**MEAN STACK**](https://github.com/linnovate/mean)
The MEAN name was coined by Valeri Karpov.
Initial concept and development was done by Amos Haviv and sponsered by Linnovate.
Inspired by the great work of Madhusudhan Srinivasa.